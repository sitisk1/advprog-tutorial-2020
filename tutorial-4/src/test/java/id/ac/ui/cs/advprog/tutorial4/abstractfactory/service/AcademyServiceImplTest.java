package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.plaf.synth.SynthButtonUI;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests
    @Test
    public void testMakeKnight(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        assertEquals(2,academyService.getKnightAcademies().size());
        academyService.produceKnight("Lordran", "synthetic");
        assertTrue(academyService.getKnight() instanceof SyntheticKnight);

    }
}
