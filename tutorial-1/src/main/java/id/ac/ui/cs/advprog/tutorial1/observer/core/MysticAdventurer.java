package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                this.guild.add(this);
                //ToDo: Complete Me
        }
        @Override
        public void update() {
                String quest = this.guild.getQuestType();
                if(quest.equals("E") || quest.equals("D")){
                        this.getQuests().add(this.guild.getQuest());
                }
        }
        //ToDo: Complete Me
}
