package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
    public String defend() {
        return "Make a barrier";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
}
