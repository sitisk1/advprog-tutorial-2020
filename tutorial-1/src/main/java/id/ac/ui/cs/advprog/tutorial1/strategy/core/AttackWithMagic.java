package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
    public String attack() {
        return "Cast a super-tier level spell";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
