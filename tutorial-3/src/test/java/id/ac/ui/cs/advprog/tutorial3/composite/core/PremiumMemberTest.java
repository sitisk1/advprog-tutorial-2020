package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member lala = new OrdinaryMember("Lala", "lala");
        member.addChildMember(lala);

    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member lala = new OrdinaryMember("Lala", "lala");
        member.addChildMember(lala);
        member.removeChildMember(lala);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member lala = new OrdinaryMember("Lala", "lala");
        Member lala1 = new OrdinaryMember("Lala", "lala");
        Member lala2 = new OrdinaryMember("Lala", "lala");
        Member lala3 = new OrdinaryMember("Lala", "lala");
        member.addChildMember(lala);
        member.addChildMember(lala1);
        member.addChildMember(lala2);
        member.addChildMember(lala3);
        assertEquals(member.getChildMembers().size(), 3);

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member member = new PremiumMember("Tata", "Master");
        Member lala = new OrdinaryMember("Lala", "lala");
        Member lala1 = new OrdinaryMember("Lala", "lala");
        Member lala2 = new OrdinaryMember("Lala", "lala");
        Member lala3 = new OrdinaryMember("Lala", "lala");
        Member lala4 = new OrdinaryMember("Lala", "lala");
        member.addChildMember(lala);
        member.addChildMember(lala1);
        member.addChildMember(lala2);
        member.addChildMember(lala3);
        member.addChildMember(lala4);
        assertEquals(member.getChildMembers().size(), 5);
    }
}
