package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = new Gun();
        weapon2 = new Sword();
        weapon3 = new Shield();
        weapon4 = new Longbow();
        weapon5 = new Gun();

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon1 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon1);

        //TODO: Complete me
        assertEquals(weapon1.getWeaponValue(), 80);
        assertEquals(weapon2.getWeaponValue(),40);
        assertEquals(weapon3.getWeaponValue(), 11);
        assertEquals(weapon4.getWeaponValue(), 20);
        assertEquals(weapon1.getWeaponValue(),80);

    }

}
