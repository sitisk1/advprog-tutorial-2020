package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member lala = new OrdinaryMember("Lala", "lala");
        guild.addMember(guildMaster,lala);
        assertEquals(guild.getMemberList().size(), 2);

    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member lala = new OrdinaryMember("Lala", "lala");
        guild.addMember(guildMaster,lala);
        assertEquals(guild.getMemberList().size(), 2);
        guild.removeMember(guildMaster,lala);
        assertEquals(guild.getMemberList().size(),1);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member lala1 = new OrdinaryMember("Lala", "lala");
        guild.addMember(guildMaster,lala1);
        assertEquals(guild.getMemberList().get(1), lala1);
    }
}
