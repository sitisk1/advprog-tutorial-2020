package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    public String name;
    public String role;
    private List<Member> memberPremium;
    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        this.memberPremium = new ArrayList<>();
    }
    public List<Member> getMemberPremium(){
        return this.memberPremium;
    }
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {

        if (this.role.equals("Master")) {
            this.memberPremium.add(member);
        }else {
            if (this.memberPremium.size() < 3) {
                this.memberPremium.add(member);
            }
        }
    }

    @Override
    public void removeChildMember(Member member) {
        this.memberPremium.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.memberPremium;
    }
    //TODO: Complete me
}
