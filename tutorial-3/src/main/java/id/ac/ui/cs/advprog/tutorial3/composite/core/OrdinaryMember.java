package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    public String name;
    public String role;
    private List<Member> memberOrdinary;
    public OrdinaryMember(String name, String role){
        this.name = name;
        this.role = role;
        this.memberOrdinary = new ArrayList<>();
    }
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        return;
    }

    @Override
    public void removeChildMember(Member member) {
        return;
    }

    @Override
    public List<Member> getChildMembers() {
        return this.memberOrdinary;
    }
    //TODO: Complete me

}
