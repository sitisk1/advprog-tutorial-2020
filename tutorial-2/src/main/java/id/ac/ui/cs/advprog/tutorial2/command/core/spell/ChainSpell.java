package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    public ArrayList<Spell> allSpells;

    public ChainSpell(ArrayList<Spell> allSpells){
        this.allSpells = allSpells;
    }
    @Override
    public void cast() {
        for(Spell spell:allSpells){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int i=allSpells.size()-1; i>=0; i--){
            allSpells.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

}
